import UIKit

/**
 Кортежи (Tuples).
 
 Кортеж — это особый объект, который группирует значения различных типов в пределах одного составного значения.
 Кортеж может содержать значения разных типов данных, так же может содержать вложенный кортеж.
 СИНТАКСИС
    var exampleTupleVar = (value_1, value_2, value_3)
 */
var example = (1, 2, 3)

//Можно объявить типы данных в кортеже для значений
var exampleTupleWithType: (Int, String, Bool) = (100, "String", true)


/**
 Обращаться к значениям кортежа можно несколькими способами:
    1. По индекску, нумерация индексов идет с нуля.
    2. Если было создано имя для доступа к элементу, можно обращаться по нему
 */

var serverResponse = (status: 200, message: "Connection successfull!!!", error:[])
var serverResponseAlt: (status: Int, message: String, error: Array) = (status: 200, message: "Connection successfull!!!", error:[])
serverResponse.message
serverResponseAlt.message

/**
 Сравнение кортежей.
 Кортежи можно сравнивать между собой, сравнение кортежей идет поэлементно до тех пор пока не будет найдено несоотвествие.
 Встроенные﻿ механизмы﻿ Swift﻿ позволяют﻿ сравнивать﻿ кортежи﻿ с ﻿количеством ﻿элементов﻿ менее﻿ 7.
 */
("String", 5) == ("String", 5) // True
let condition = ("String!!!", 5) == ("String", 5) // False
/************** Typles end ****************/

/**
 Условия.
 
 */
assert(condition, true, "Error")

/***************************************************************
 *                                                             *
 *                  Домашнее задание. Модуль 2                 *
 *                                                             *
 ***************************************************************/

